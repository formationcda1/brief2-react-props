import { useState } from 'react'
import { Contact } from './components/Contact'
import './App.css'



function App() {
 

  return (
    <div>
      <h1> Mes Contacts</h1>
      <div className='contact-container'>
      <Contact profil="https://www.playcentral.de/wp-content/uploads/2023/05/natalie-portman-star-wars-padme.jpg" name="Marine" status ="En ligne"/>
      <Contact profil="https://www.starwars-universe.com/images/actualites/episode9/anakin_rots2.jpg" name="Bastien" status = "Hors ligne"/>
      <Contact profil="https://img.lemde.fr/2017/12/07/0/0/3500/2333/664/0/75/0/3892f92_PJ401_FILM-STARWARS-ALIENLIFE_1207_11.JPG" name="Robert" status ="Occupé"/>
      <Contact profil="https://i0.wp.com/cms.babbel.news/wp-content/uploads/2019/12/Jabba-the-Hutt-Credit-StarWars.comLucasfilm-Ltd..jpg?resize=1200%2C675" name="Enrico" status ="Occupé"/>
      <Contact profil="https://www.leparisien.fr/resizer/Zvp9EtUTx2iqn1Jz7ipBH8PWFw4=/932x582/arc-anglerfish-eu-central-1-prod-leparisien.s3.amazonaws.com/public/JDMVIMTE6NBLPQL76FMQJ2LFIM.jpg" name="Jean-Rémi" status ="En ligne"/>
      <Contact profil="https://www.dhnet.be/resizer/QTlW2lr7zgRo7QQaqiXxpufLigA=/1200x800/filters:format(jpeg):focal(328.5x255:338.5x245)/cloudfront-eu-central-1.images.arcpublishing.com/ipmgroup/FNFSL7HO4RBSRIOJGRODYNG35Y.jpg" name="Patrick" status ="En ligne"/>

      </div>
      
    
    </div>
      
  )
}

export default App
